﻿using UnityEngine;
using System.Collections;

public class fixSortingInLayer : MonoBehaviour {

    public SpriteRenderer render;
	// Use this for initialization
	void Start () {
        render = GetComponent<SpriteRenderer>();
        this.render.sortingOrder = (int) (Mathf.RoundToInt(transform.position.y) + Mathf.RoundToInt(transform.position.x)) + Random.Range(0, 1000);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
