﻿using UnityEngine;
using System.Collections;

//Brenden Greenlee
//1.00

/*Recieves new character attributes from level up UI
 * holds them here before sending the new physical stats 
 * to their relevant scripts
 */
 
public class char_att : MonoBehaviour {

	//main attributes
	private int strength;
	private int dexterity;
	private int endurance;
	private int recovery;
	private int constitution;
	private int agility;
	private int speed;
	public SoundEffect effect;
	public AudioSource[] arr = new AudioSource[3];

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void levelUpCall()
	{
		Debug.Log ("leveled up");
	}

//	public static void levelUp(int newStr, int newDex, int newEnd, int newRec, int newCon, int newAgi, int newSpe)
//	{
//
//		strength = newStr;
//		applyStrength (0);
//	
//		dexterity = newDex;
//		applyDexterity (0);
//
//		endurance = newEnd;
//		applyEndurance (0);
//
//		recovery = newRec;
//		applyRecovery (0);
//
//		constitution = newCon;
//		applyConstitution(0);
//
//		agility = newAgi;
//		applyAgility (0);
//
//		speed = newSpe;
//		applySpeed (0);
//
//	}
//
//	private void applyStrength(int strMod)
//	{
//		
//	}
//	private void applyDexterity(int dexMod)
//	{
//		
//	}
//	private void applyEndurance(int endMod)
//	{
//		
//	}
//	private void applyRecovery(int recMod)
//	{
//		
//	}
//	private void applyConstitution(int conMod)
//	{
//
//	}
//	private void applyAgility(int agiMod)
//	{
//		easyMove.setInvulTime (agiMod);
//		easyMove.setRollSpeed (agiMod);
//	}
//	private void applySpeed(int speMod)
//	{
//		easyMove.setSpeed (speMod);
//	}
}
