﻿using UnityEngine;
using System.Collections;

public class char_enemy : npc {

	public int health = 100;
	private float angle;
	private GameObject clone;
	public GameObject spear;
	//private GameObject =
	// Use this for initialization
	void Start () {
		setTarget (GameObject.Find ("Char"));

	}
	
	// Update is called once per frame
	void Update () {
		Vector3 dir = getTar().transform.position - transform.position;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg ;
		transform.rotation = Quaternion.AngleAxis(angle - 90.0f, Vector3.forward);
		//tarLoc = getTar ().transform.localPosition;
		//look (tarLoc);
		if (Input.GetKeyDown (KeyCode.P))
			attack ();
	
	
	}

	private void attack()
	{
		clone = Instantiate (spear, transform.position, transform.rotation) as GameObject;
		clone.GetComponent<Rigidbody2D> ().velocity = (transform.up) * 1.0f;
	}

	public void hit(float damage)
	{
		health -= (int)damage;
	}

}
