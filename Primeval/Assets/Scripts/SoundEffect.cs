﻿using UnityEngine;
using System.Collections;

public class SoundEffect : MonoBehaviour {

	AudioSource[] arr;

	public SoundEffect(int numb)
	{
		arr = new AudioSource[numb];
	}

	public void set(int index, AudioSource clip)
	{
		arr [index] = clip;
	}

	public void play()
	{
		arr [Random.Range (0, arr.Length)].Play ();

	}
}
