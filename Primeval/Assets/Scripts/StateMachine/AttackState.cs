﻿using UnityEngine;
using System.Collections;
using System;

public class AttackState : IEnemyState
{
    private readonly StatePatternEnemy enemy;

    public AttackState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
    }

    public void UpdateState()
    {
        enemy.moveTO();
        Debug.Log("Enemy is Currently Attacking");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        //ToAlertState();
    }

    public void OnTriggerStay2D(Collider2D other)
    {

    }

    public void OnTriggerExit2D(Collider2D other)
    {

    }

    public void ToIdleState()
    {

    }

    public void ToAlertState()
    {
        //enemy.currentState = enemy.alertState;
    }

    public void ToAttackState()
    {

    }

    public void ToChaseState(Transform target)
    {

    }
}