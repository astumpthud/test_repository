﻿using UnityEngine;
using System.Collections;

public class ChaseState : IEnemyState
{
	private readonly StatePatternEnemy enemy;

	public ChaseState(StatePatternEnemy statePatternEnemy) 
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
    {
        Debug.Log("Enemy is Chasing");
        float distance = enemy.checkDistance(enemy.chaseTarget);

        if(distance <= enemy.attackRange)
        {
            ToAttackState();
        }

        else
        {
            enemy.moveTO();
        }

        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

	public void OnTriggerStay2D(Collider2D other)
	{

	}

	public void OnTriggerExit2D(Collider2D other)
	{

	}

	public void ToIdleState()
    {

    }

    public void ToAlertState()
    {

    }

    public void ToAttackState()
    {
        enemy.currentState = enemy.attackState;
    }

    public void ToChaseState(Transform target)
    {
        // Current State
    }
}