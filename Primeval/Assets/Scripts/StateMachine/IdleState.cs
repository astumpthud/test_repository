﻿using UnityEngine;
using System.Collections;

public class IdleState : IEnemyState
{
	private readonly StatePatternEnemy enemy;

	public IdleState(StatePatternEnemy statePatternEnemy) 
	{
		enemy = statePatternEnemy;
	}

    public void UpdateState()
    {
		Debug.Log("Enemy is Currently Idling");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
		ToAlertState();
	}

	public void OnTriggerStay2D(Collider2D other)
	{

	}

	public void OnTriggerExit2D(Collider2D other)
	{

	}

	public void ToIdleState()
    {

    }

    public void ToAlertState()
    {
		enemy.currentState = enemy.alertState;
    }

    public void ToAttackState()
    {

    }

    public void ToChaseState(Transform target)
    {

    }
}