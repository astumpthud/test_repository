﻿using UnityEngine;
using System.Collections;

public interface IEnemyState
{

    void UpdateState();

    void OnTriggerEnter2D(Collider2D other);

	void OnTriggerStay2D(Collider2D other);

	void OnTriggerExit2D(Collider2D other);

    void ToIdleState();

    void ToAlertState();

    void ToAttackState();

    void ToChaseState(Transform target);
}
