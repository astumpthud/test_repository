﻿using UnityEngine;
using System.Collections;

public class AlertState : IEnemyState
{
	private readonly StatePatternEnemy enemy;

	public AlertState(StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
    {
		Debug.Log("Enemy is Alert");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        
    }

	public void OnTriggerStay2D(Collider2D other)
	{
        ToChaseState(other.transform);
	}

	public void OnTriggerExit2D(Collider2D other)
	{
		ToIdleState();
	}

	public void ToIdleState()
    {
		enemy.currentState = enemy.idleState;
    }

    public void ToAlertState()
    {

    }

    public void ToAttackState()
    {

    }

    public void ToChaseState(Transform target)
    {
        enemy.chaseTarget = target.transform;
        enemy.currentState = enemy.chaseState;
    }
}
