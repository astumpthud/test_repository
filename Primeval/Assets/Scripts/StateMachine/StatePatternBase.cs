﻿using UnityEngine;
using System.Collections;

public class StatePatternBase : MonoBehaviour
{
    [HideInInspector] public Transform chaseTarget;
    // TODO: Implement path finding
    public void moveTO()
    {
        // TODO: rigid body add force for movement
        this.transform.position = Vector2.MoveTowards(this.transform.position, chaseTarget.position, 1 * Time.deltaTime);
    }

    public float checkDistance(Transform other)
    {
        return Vector2.Distance(other.position, transform.position);
    }
}
