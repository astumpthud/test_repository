﻿using UnityEngine;
using System.Collections;

public class StatePatternEnemy : StatePatternBase
{

    public float searchingTurnSpeed = 120f;
    public float searchingDuration = 4f;
    public float sightRange = 20f;

    public float shortAttackRange = .5f;
    public float midAttackRange;
    public float longAttackRange;

    // TODO: Can't add to array in default inspector either custom inspector with serialized objects or use a separate script to pull in attacks
    public AttackState[] shortAttacks;
    public AttackState[] midAttacks;
    public AttackState[] longAttacks;

    public MeshRenderer meshRendererFlag;

    [HideInInspector] public IEnemyState startState;
    [HideInInspector] public IEnemyState currentState;
    [HideInInspector] public IEnemyState idleState;
    [HideInInspector] public IEnemyState alertState;
    [HideInInspector] public IEnemyState chaseState;
    [HideInInspector] public IEnemyState attackState;

    [HideInInspector] public float attackRange;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    void Awake()
    {
		idleState = new IdleState(this);
		alertState = new AlertState(this);
		chaseState = new ChaseState(this);
        attackState = new AttackState(this);

		navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start ()
	{
        startState = alertState;
		currentState = startState;

        if(float.IsNaN(midAttackRange))
        {
            midAttackRange = 2 * shortAttackRange;
        }

        if(float.IsNaN(longAttackRange))
        {
            longAttackRange = 3 * shortAttackRange;
        }

        if (longAttacks != null && longAttacks.Length > 0)
        {
            attackRange = longAttackRange;
        }

        else if (midAttacks != null && midAttacks.Length > 0)
        {
            longAttackRange = 0;
            attackRange = midAttackRange;
        }

        else
        {
            longAttackRange = 0;
            midAttackRange = 0;
            attackRange = shortAttackRange;
        }
	}
	
	// Update is called once per frame
	void Update ()
	{
        if(chaseTarget != null)
        {
            if(checkDistance(chaseTarget) > attackRange)
            {
                deAggro();
            }
        }

		currentState.UpdateState();
	}

    public void deAggro()
    {
        chaseTarget = null;
        currentState = startState;
    }

	private void OnTriggerEnter2D(Collider2D other)
	{
		currentState.OnTriggerEnter2D(other);
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		currentState.OnTriggerStay2D(other);
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		currentState.OnTriggerExit2D(other);
	}
}
