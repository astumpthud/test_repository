﻿using UnityEngine;
using System.Collections;

public class npc : MonoBehaviour {

	private GameObject target;
	public Vector2 tarLoc;
	private SpriteRenderer sr;
	private Rigidbody2D rb;
	private AudioSource myAudio;
	private int move_dir;
	private double move_tan;
	private double move_speed;
	private bool chasing = false;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		sr = GetComponent<SpriteRenderer> ();
		//GetComponent<AudioSource>() = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
//	void FixedUpdate ()
//	{
//
//		move_tan = Mathf.Atan2 (rb.velocity.y, rb.velocity.x);
//		if (chasing)
//			transform.position = Vector2.MoveTowards (transform.position, target.transform.position, (float)move_speed * Time.deltaTime);
//
//		if (move_tan >= Mathf.PI / -8 && move_tan <= Mathf.PI / 8) {
//			//sprite renderer = 2
//		} else if (move_tan >= Mathf.PI * 3/8 && move_tan <= Mathf.PI * 5/8) {
//			//sprite renderer = 1
//		} else if (move_tan >= Mathf.PI * 7/8 || move_tan <= Mathf.PI * -7/8) {
//			//sprite renderer = 4
//		} else if (move_tan >= Mathf.PI * -5/8 && move_tan <= Mathf.PI * -3/8) {
//			//sprite renderer = 3
//		} 
//	}

	public void moveTo(GameObject obj)
	{
		target = obj;
		chasing = true;
	}

	public void setTarget(GameObject obj)
	{
		target = obj;
	}

	public GameObject getTar()
	{
		return target;
	}

	public void play(AudioClip[] clip)
	{
		GetComponent<AudioSource>().clip = clip [Random.Range (0, clip.Length)];
		GetComponent<AudioSource>().Play ();
	}

}
