﻿using UnityEngine;
using System.Collections;

public class spear_proj : MonoBehaviour {

	private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		
		if (collision.gameObject.tag == "Boom")
			Destroy (this.gameObject);
	
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player") {
			HUD_test.loose_hp (15);
			collision.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 ());
			Destroy (this.gameObject);

		}
	}

}
