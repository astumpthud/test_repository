﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;


public class spear_test : MonoBehaviour {

	private float dmg = 1.0f;
	private float dmgMod = 1.0f;
	public float speed = 8.0f;
	private GameObject clone;
	private SpriteRenderer cloneSR;
	private GameObject spear;
	public GameObject boomerang;
	private Vector3 target_dir = new Vector3();
	public static SpriteRenderer sr;

	private float distortion = 0f;
	//spublic AudioMixer mixer;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		//mixer = GetComponent<AudioMixer> ();
	}
		

	// Update is called once per frame
	void Update () {

		if (sr.enabled) {
			distortion = 10f;
			//mixer.SetFloat ("Music Dist", distortion);
		}


		Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint (transform.position);
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg ;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		if (Input.GetMouseButtonUp (0)) {
			attackL ();

		}
		if (Input.GetMouseButton (0)) {
			HUD_test.halt_regen ();
			if (dmg < 2.0f)
				dmg += 0.02f;
		}

		if (Input.GetMouseButtonDown (1)) {
			attackR ();
		}
		//Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
		//transform.LookAt (mousePos);
	}

	void attackL(){
		//Debug.Log ("left click");
		if (sr.enabled == true)
			shoot ();
		else if (!clone.GetComponent<boom_test> ().isReturning ())
			shoot(clone.transform.position);
			shoot_fail ();
	}

	public void pick_up(){
		sr.enabled = true;
		Destroy (clone);
	}
		
	void shoot() {
		dmgMod = 1.0f;
		HUD_test.resume_regen ();
		HUD_test.loose_stam ((int) dmg * 2);
		sr.enabled = false;
		clone = Instantiate (boomerang, transform.position, transform.rotation) as GameObject;
		clone.GetComponent<SpriteRenderer> ().enabled = true;
		clone.GetComponent<Rigidbody2D>().velocity = transform.right * speed * dmg;
		clone.GetComponent<boom_test> ().StartCoroutine ("boom_return");
		clone.SendMessage ("setDamage", dmg * dmgMod);
		dmg = 1.0f;
		//clone.GetComponent<Rigidbody2D> ().angularVelocity = spinSpeed;

	}
	void shoot(Vector3 boomLocation){
		dmgMod *= 0.75f;
		HUD_test.resume_regen ();
		HUD_test.loose_stam ((int) dmg * 2);
		clone = Instantiate (boomerang, boomLocation, transform.rotation) as GameObject;
		clone.GetComponent<SpriteRenderer> ().enabled = true;
		Vector3 cloneVelocity = (Input.mousePosition - Camera.main.WorldToScreenPoint(boomLocation)).normalized;
		clone.GetComponent<Rigidbody2D>().velocity = cloneVelocity * speed * dmg;
		clone.GetComponent<boom_test> ().StartCoroutine ("boom_return");
		Debug.Log (dmg);
		clone.SendMessage ("setDamage", dmg * dmgMod);
		dmg = 1.0f;

		//Instantiate (boomerang, boomClone.transform.position, transform.rotation) as GameObject;

	}
	void shoot_fail(){

	}
	void attackR() {
		//Debug.Log ("right Click");
	}
}
