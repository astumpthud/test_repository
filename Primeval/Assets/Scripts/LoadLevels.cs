﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour {

    static bool ran = false;

	// Use this for initialization
	void Start () {

        if (!ran)
        {
            ran = true;
            // TODO: Only Trigger if the script is called by the active scene
            Scene activeScene = SceneManager.GetActiveScene();

            GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("spawnPoint");
            GameObject sceneSpawnPoint = null;

            foreach (GameObject spawnPoint in spawnPoints)
            {
                if (spawnPoint.scene.name == activeScene.name)
                {
                    sceneSpawnPoint = spawnPoint;
                    break;
                }
            }

            if (activeScene.name != "MOUNTAIN12345")
            {
                SceneManager.LoadScene("MOUNTAIN12345", LoadSceneMode.Additive);
            }

            if (activeScene.name != "tropical forest copy")
            {
                SceneManager.LoadScene("tropical forest copy", LoadSceneMode.Additive);
            }

            if (activeScene.name != "grassland")
            {
                SceneManager.LoadScene("grassland", LoadSceneMode.Additive);
            }

            if (activeScene.name != "village")
            {
                SceneManager.LoadScene("village", LoadSceneMode.Additive);
            }

            // TODO: Spawn in Player
            // Getting Null Multiple Chars??
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            Debug.Log(player);
            //player.transform.position = sceneSpawnPoint.transform.position;
        }
    }
}
