﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class easyMove : MonoBehaviour {

	private bool isRolling = false;
	public static float rollSpeed = 60.0f;
	public static float maxRollSpeed = 3.0f;
	public static float minRollSpeed = 1.5f;
	public float recoverSpeed = 0.1f;
	public float speed = 50.0f;
	private float speed_mod;
	public static float maxSpeed = 50;
	public static float minSpeed = 5;
	public static float invulTime = .1f;
	public static float maxInvulTime = 0.25f;
	public static float minInvulTime = 0.1f;
	private float velAngle = 0.0f;
	private static float pi = Mathf.PI;

	private float movex = 0f;
	private float movey = 0f;

	public static SpriteRenderer sr;
	public static Animator ar;
	private Vector2 mouse;
	private float tan;
	private float vtan;
	private float rad1 = pi * 7/8;
	private float rad2 = pi * 5 / 8;
	private float rad3 = pi * 3 / 8;
	private float rad4 = pi * 1/8;
	private float rad5 = pi * -1 / 8;
	private float rad6 = pi * -3 / 8;
	private float rad7 = pi * -5 / 8;
	private float rad8 = pi * -7 / 8;
	private static int rHeight;
	private static int rWidth;
	private static GameObject spear;
	private float time;
	private Vector2 vel;
	private Rigidbody2D rb;

	private AudioSource footstep;
	public AudioMixer mixer;
	private bool hasStepped = false;
	private float stepTime = 0.4f;
	private float lastStep;

	// Use this for initialization
	void Start () {
		footstep = GetComponent<AudioSource> ();
		rb = GetComponent<Rigidbody2D> ();
		sr = GetComponent<SpriteRenderer>();
		ar = GetComponent<Animator>();
		rHeight = Screen.height;
		rWidth = Screen.width;
	}

	// Update is called once per frame
	void Update ()
	{
		mouse.x = Input.mousePosition.x - rWidth / 2;
		mouse.y = Input.mousePosition.y - rHeight / 2;
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");

		tan = Mathf.Atan2 (mouse.y, mouse.x);
		vtan = Mathf.Atan2 (Mathf.Abs(movey), Mathf.Abs(movex) );

		if (Input.GetButtonDown ("Jump")) {
			StartCoroutine("roll");
			//Debug.Log (mixer.name);
		}

		//ar.SetFloat ("anim_speed", vtan * 100);

		if (Input.GetMouseButtonDown (0))
			rb.drag = 30;
		if (Input.GetMouseButtonUp (0))
			rb.drag = 10;
		
		if (isRolling == false) {
			if (movex != 0.0f || movey != 0.0f)
				ar.SetFloat ("anim_speed", 1.0f);
			else
				ar.SetFloat ("anim_speed", 0.0f);
			rb.AddForce ( new Vector2 (movex * speed *   Mathf.Cos(vtan)  , Mathf.Sin(vtan)*  movey * speed  ) );

			//sets the sprite based on mouse direction
			if (tan > rad1) {
				ar.SetInteger ("direction", 6);
			} else if (tan > rad2) {
				ar.SetInteger ("direction", 7);
			} else if (tan > rad3) {
				ar.SetInteger ("direction", 0);
			} else if (tan > rad4) {
				ar.SetInteger ("direction", 1);
			} else if (tan > rad5) {
				ar.SetInteger ("direction", 2);
			} else if (tan > rad6) {
				ar.SetInteger ("direction", 3);
			} else if (tan > rad7) {
				ar.SetInteger ("direction", 4);
			} else if (tan > rad8) {
				ar.SetInteger ("direction", 5);
			} else
				ar.SetInteger ("direction", 6);

		}
			
		if (hasStepped && rb.velocity.magnitude > 0f) {
			footstep.Play ();
			hasStepped = false;
		} else if (movex == 0f && movey == 0f) {
			//footstep.Stop ();
		} else {
			if (Time.time - lastStep >= stepTime) {
				lastStep = Time.time;
				hasStepped = true;
			}

		}
			
	}
		

	private IEnumerator roll() 
	{
		HUD_test.loose_stam(10);
		isRolling = true;
		rb.AddForce (new Vector2(movex,movey).normalized * 2000.0f);
		yield return new WaitForSeconds (0.25f);
		yield return new WaitForSeconds (0.15f);
		isRolling = false;
	}

	private IEnumerator hitAnim(float time)
	{
		sr.color = new Color (255f, 0f, 0f);
		yield return new WaitForSeconds (time);
		sr.color = new Color (255f, 255f, 255f);
	}

	public void hit(int damage, Vector2 direction, int force)
	{
		StartCoroutine (hitAnim (.25f));
		rb.AddForce (direction.normalized * force);
		Debug.Log (damage);
	}
		
}