﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD_test : MonoBehaviour {

	private static Slider sBar;
	private static Slider hpBar;
	private float regenTime = 0.75f;
	private static bool isRegen = true;
	private static bool regenPause = false;
	// Use this for initialization
	void Start () {
		sBar = GameObject.Find ("Stam_bar").GetComponent<Slider>();
		hpBar = GameObject.Find ("HP_bar").GetComponent<Slider> ();
		//hpBar.value = 50;
	}

	private IEnumerator stamRegen() {
		isRegen = false;
		yield return new WaitForSeconds (regenTime);
		gain_stam (1);
		isRegen = true;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (isRegen && sBar.value < sBar.maxValue) {
			StartCoroutine ("stamRegen");
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			loose_hp (10);
		}
	}
	public static void loose_hp( int lostHP)
	{
		hpBar.value -= lostHP;
	}
	public static void loose_stam (int lostStam) 
	{
		sBar.value -= lostStam;
	}
	public static void gain_stam( int gainStam)
	{
		sBar.value += gainStam;
	}

	public static void halt_regen()
	{
		isRegen = false;
	}

	public static void resume_regen()
	{
		isRegen = true;
	}



}